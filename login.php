<?php
    require "template/template.php";

    function getTitle(){
        echo "What To Do | Login";
    }

    function getContent(){
    ?>
    <div class="d-flex justify-content-center align-items-center flex-column" id="login-container">
    <img src="assets/images/logo.png" id="applogo" class="mt-5">
    <h1 class="py-5">Login</h1>
        <form action="controllers/process_login.php" method="POST">
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" name="email" class="form-control">
            </div>
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" name="password" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary">Login</button>
        </form>
        <p class="py-3">New user? <a class="text-info" href="register.php">Register</a></p>
    </div>
<?php    
    }
?>