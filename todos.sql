Mark as Done, show Done, Undone tasks

CREATE TABLE categories (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255),
	PRIMARY KEY (id)
);

CREATE TABLE statuses (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255),
	PRIMARY KEY (id)
);

CREATE TABLE roles (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE todos (
id INT NOT NULL AUTO_INCREMENT,
todo VARCHAR(65255),
date VARCHAR(255),
category_id int,
status_id int,
PRIMARY KEY (id),
foreign key(category_id) references categories(id) on update cascade on delete restrict,
foreign key(status_id) references statuses(id) on update cascade on delete restrict
);

CREATE TABLE users (
	id int not null AUTO_INCREMENT,
	firstName VARCHAR(255),
	lastName VARCHAR(255),
	email VARCHAR(255),
	password VARCHAR(255),
	role_id int,
	PRIMARY KEY (id),
	foreign key(role_id) references roles(id) on update cascade on delete restrict
);