<?php
  require "template/navbar.php";
  require "template/sidepanel.php";
  require "template/template.php";
  


  function getTitle(){
    echo "WhatToDo | Add Task";
  }

  function getContent(){
    ?>
<div class="col-lg-9 offset-lg-3">
<h1 class="text-center py-5">Add Task Form</h1>

<div class="d-flex justify-content-center align-items-center" id="divInAddTaks">
    <form action="controllers/process_add_task.php" method="POST" class="" enctype="multipart/form-data">

        <!-- input of the task -->
        <div class="form-group">
            <label for="task" id="addForm">What to do?</label>
            <input type="text" name="task" class="form-control" id="input" placeholder="Input Task">
        </div>

        <!-- input of deadline -->
        <div class="form-group">
            <label for="task" id="addForm">Deadline</label>
            <input type="date" name="deadline" class="form-control" id="input" placeholder="Input Deadline">
        </div>

        <!-- input of category -->
        <div class="form-group">
            <label for="category_id" id="addForm">Category: </label>
            <select name="cat_id" class="form-control" id="input">
                <?php
    require "controllers/connection.php";

    $category_query = "SELECT * FROM categories";
    $categories = mysqli_query($conn, $category_query);


    foreach($categories as $indivCategory){
        ?>
        <option value="<?= $indivCategory['id'] ?>"><?= $indivCategory['name'] ?></option>
        <?php
        }
        ?>
          </select>
        </div>
        <!-- button submit -->
        <button class="btn btn-info" type="submit">Add Task</button>
    </form>
  </div>
</div> 
<?php
}

?>
