<?php
    require "template/template.php";

    function getTitle(){
        echo "What To Do";
    }

    function getContent(){
        ?>

		<div class="landing-container d-flex justify-content-center align-items-center flex-column vh-100">
			<img class="landinglogo" src="assets/images/logo.png" width="300">
			<br>
			<a href="register.php">
				<button class="landingBtn btn btn-inverse btn-warning mb-2" ><strong>Let's Do This!</strong></button>
			</a>
		</div>

<?php    
    }
?>
