<nav id="sidebar" class="text-center">
        <div class="sidebar-header">
            <img src="../assets/images/boy.png" alt="">
            <h1 class="mt-4 blue">Hi, <?= $_SESSION['user']['firstName']?>!</h1>
        </div>

        <ul class="list-unstyled components">
            <li class="blacksm">
                <a href="#homeSubmenu">Today</a>
            </li>
            <li class="blacksm">
                <a href="#">Next 7 days</a>
            </li>
            <li class="blacksm">
                <a href="#">IMPORTANT</a>
            </li>
        </ul>
        <hr class="bg-primary">
        <div class="wrapper">
        <h2 class="text-left pl-4">Work</h2>
            <ul class="list-unstyled components text-left pl-5">
                <li class="active">
                    <a href="#" class="pill">Website</a>
                </li>
                <li>
                    <a href="#" class="pill">Zuitt</a>
                </li>
            </ul>
        <h2 class="text-left pl-4">Personal</h2>
        <ul class="list-unstyled components  text-left pl-5">
            <li class="active">
                <a href="#" class="pill">Grocery</a>
            </li>
            <li>
                <a href="#" class="pill">Reading List</a>
            </li>
        </ul>
        </div>
            <a href="../controllers/process_logout.php"class="btn btn-logout">Logout</a>
    </nav>
    

    