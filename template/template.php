<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- bootswatch -->
    <link rel="stylesheet" href="https://bootswatch.com/4/materia/bootstrap.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!--  jquery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" defer></script>

    <!--  toastr js-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" defer></script>

    <!--  Registration Validation Script-->
    <script src="../assets/scripts/register.js" defer></script>

    <!--  toastr css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous" defer ></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous" defer></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous" defer></script>
    
    <script src="https://kit.fontawesome.com/7322aed6b3.js" crossorigin="anonymous" defer></script>


    <title>
    <?php getTitle(); ?>
    </title>
</head>
<body>
        <!-- This is where we want to insert the content of the pages -->
        <!-- We will create a function in every page, to get the content of that page -->


        <?php
            getContent();
        ?>

        
    </div>
</body>
</html>
