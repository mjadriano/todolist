<?php
  require "template/navbar.php";
  require "template/sidepanel.php";
  require "template/template.php";
  


  function getTitle(){
    echo "What To Do - Your To Do List App";
  }

  function getContent(){
  //We need to call the connection file whenever we need to access the db
  require "controllers/connection.php";

//    In here we want to publish the content of the index.php
    ?>
<!-- Catalog Body -->
<div class="container my-4" id="page-bg">
    <div class="row">
        <div class="col-lg-9 offset-lg-3 bg-secondary">
          <div class="d-flex justify-content-between align-items-center">
          <a href="add-task.php" class="btn btn-add mt-2">Add Task</a>
          <a href="">All To Dos</a>
          <a href="">Mark as done</a>
          <a href="">Not yet done</a>
          <div class=""><i class="fas fa-sliders-h"></i></div>
        </div>
        <hr>
        <div class="table-responsive col-lg-12">
        <div class="container">New Task</div>
        <?php 
        //query todos
        $todos_query = "SELECT * FROM todos";
        $todos = mysqli_query($conn, $todos_query);
        // var_dump($todos);
        foreach($todos as $todo){
          ?>
              <div class="container">
              <div class="d-flex bd-highlight border-bottom align-items-center">
                <div class="p-2 bd-highlight"><input type="checkbox"></div>
                <div class="p-2 flex-grow-1 bd-highlight"><?= $todo['todo'];?></div>
                <div class="p-2 bd-highlight"></div>
                <?php
                $categoryId = $todo['category_id'];
                $category_query = "SELECT * FROM categories WHERE id = $categoryId";
                $category = mysqli_fetch_assoc(mysqli_query($conn, $category_query));
                ?>
                <div class="p-2 flex-shrink-1 bd-highlight"><a class="pill" href="#"><?= $category['name']?></a></div>
                <div class="p-2 flex-shrink-1 bd-highlight"><?= $todo['date']; ?></div>
                <a href="edit-todo.php?todo_id=<?= $todo['id'] ?>" class="btn btn-warning m-1" style="width:100px">Edit</a>
                <a href="controllers/process_delete_todo.php?todo_id=<?php echo $todo['id'] ?>" class="btn btn-danger m-1" style="width:100px">Delete</a>
              </div>
        </div>

        <?php
        }
        ?>
  </div>
</div>
  <?php
  }
?>


