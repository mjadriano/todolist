<?php
    require "template/template.php";

    function getTitle(){
        echo "WhatToDo | Register";
    }

    function getContent(){
    ?>
    <div class="d-flex justify-content-center align-items-center flex-column">
        <img src="assets/images/logo.png" id="applogo" class="mt-5">
        <h1 class="py-5">Register</h1>
        <form action="controllers/process_register.php" method="POST">
            <div class="form-group">
                <label for="firstName">
                    <span>*</span>First Name:
                </label>
                <input type="text" name="firstName" class="form-control" id="firstName" placeholder="First Name">
                <span class="text-danger"></span>
            </div>
            <div class="form-group">
                <label for="lastName">
                    <span>*</span>Last Name:
                </label>
                <input type="text" name="lastName" class="form-control" id="lastName" placeholder="Last Name">
                <span class="text-danger"></span>
            </div>
            <div class="form-group">
                <label for="email">
                    <span>*</span>Email:
                </label>
                <input type="email" name="email" class="form-control" id="email" placeholder="Email address">
                <span class="text-danger"></span>
            </div>
            <div class="form-group">
                <label for="password">
                    <span>*</span>Password:
                </label>
                <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                <span class="text-danger"></span>
            </div>
            <div class="form-group">
                <label for="confirm_password">
                    <span>*</span>Confirm Password:
                </label>
                <input type="password" name="confirm_password" class="form-control" id="confirmPassword" placeholder="Confirm Password">
                <span class="text-danger"></span>
            </div>
            <button type="button" class="btn btn-info" id="registerBtn">Register</button>
        </form>
        <p class="py-2">Already Registered? <a class="text-info" href="login.php">Login</a></p>
    </div>
<?php    
    }
?>