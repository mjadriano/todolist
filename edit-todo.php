<?php
  require "template/navbar.php";
//   require "template/sidepanel.php";
  require "template/template.php";

function getTitle(){
	echo "What To Do | Edit";
}

function getContent(){
    require "controllers/connection.php";
    $todoId = $_GET['todo_id'];
    $todo_query = "SELECT * FROM todos WHERE id = $todoId";
    //transform the result of the query into an associative array
    $todo_result = mysqli_fetch_assoc(mysqli_query($conn, $todo_query));
    
    // var_dump($todo_result);
    ?>
    <h1 class="text-center py-5">Edit To Do</h1>
	  <div class="d-flex justify-content-center align-items-center">
		<!--			We need to remember, if we want to capture data from an input type file, we need to add the attribute
		enctype="multipart/form-data" in our form tag-->
		<form action="controllers/process_edit_todo.php" method="POST" class="mb-5" enctype="multipart/form-data">
			<div class="form-group">
				<label for="todo">To Do:</label>
				<input type="text" name="todo" class="form-control" value="<?= $todo_result['todo'] ?>">
			</div>
			<div class="form-group">
				<label for="date">Deadline:</label>
				<input type="date" name="date" class="form-control" value="<?= $todo_result['date'] ?>">
			</div>
      <div class="form-group">
				<label for="category_id">Category:</label>
				<select name="category_id" class="form-control">
					<?php
					require "controllers/connection.php";

					$category_query = "SELECT * FROM categories";
					$categories = mysqli_query($conn, $category_query);

					foreach($categories as $category){

          ?>
						<option value="<?= $category['id']; ?>"
            <?= $category['id'] === $todo_result['category_id'] ? "selected" : "" ?>><?= $category['name']; ?></option>
						<?php
					}
					?>
				</select>
			</div>
      <input type="hidden" name="todo_id" value="<?= $todo_result['id']; ?>" >
		    <button class="btn btn-info" type="submit">Edit Todo</button>
		</form>
	</div>
					  
	<?php
}?>